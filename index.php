<?php

    // Реализовать алгоритм проверки контрольных сумм банковских карт:
    // 1. Проверка контрольной суммы банковских карты по стандарту ISO/IEC 7812
    // 2. Проверка типа карты. Потенциальный сервис принимает только VISA, MasterCard,
    // Maestro и Даронь Кредит.
    // 3. Префиксы для карты Даронь Кредит: 14 81 99
    // 4. Длина номера карты даронь Кредит: 14

    function luna($number)
    {
        $sum = 0;
        $len = strlen($number);
        for ($i = 0; $i < $len; $i++) {
            if ((($i + 1) % 2) == 0) {
                $val = $number[$i];
            }else{
                $val = $number[$i] * 2;
                if ($val > 9){
                    $val -= 9;
                }
            }
            $sum += $val;
        }
        return (($sum % 10) === 0);
    }


    function card_type($number){
        if(strlen($number) == 14 && substr($number, 0, 6) == "148199"){
            return True;
        }else{
            if ($number[0] == "4" || $number[0] == "5"){
                return True;
            }
        }
        return False;
    }
?>


<?php

    $card = '';
    $message = '';
    if (isset($_POST['card'])){
        $card = str_replace(' ', '', $_POST['card']);
        if(strlen($card) == 0){
            $message = 'Нет данных карты';
        }else{
            if (card_type($card)){
                if(luna($card)){
                    $message = 'Номер карты правильный';
                }else{
                    $message = 'Номер карты непарвильный';
                }
            }else{
                $message = 'Тип карты неправильный';
            }
        }
    }

?>


<!DOCTYPE html>
<html lang="ru">
    <head>
        <meta charset="UTF-8">
        <title>проверка карты</title>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-aFq/bzH65dt+w6FI2ooMVUpc+21e0SRygnTpmBvdBgSdnuTN7QbdgL+OapgHtvPp" crossorigin="anonymous">
    </head>

    <body>
        <div class = "container mt-5"> 

            <?php if(!empty($message)): ?>
                <div class="alert alert-primary" role="alert">
                    <?= $message; ?>
                </div>
            <?php endif; ?>

            <form method="POST">
                <div class="row">
                    <div class="col-10">
                        <div class="mb-3">
                            <label for="cardInput">Номер карты</label>
                            <input name="card" id="cardInput" class="form-control" type="tel" inputmode="numeric" pattern="[0-9\s]{13,19}" autocomplete="cc-number" maxlength="19" placeholder="xxxx xxxx xxxx xxxx">
                        </div>
                    </div>

                    <div class="col-2 d-flex justify-content-center">
                        <button type="submit" class="btn btn-primary">Отправить</button>
                    </div>
                </div>
            </form>
        </div>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha2/dist/js/bootstrap.bundle.min.js" integrity="sha384-qKXV1j0HvMUeCBQ+QVp7JcfGl760yU08IQ+GpUo5hlbpg51QRiuqHAJz8+BrxE/N" crossorigin="anonymous"></script>
    <body>
<html>